# Docker Hugo Image

Use in `.gitlab-ci.yml` with `image: registry.gitlab.com/kissaki/docker-hugo:latest`

# Content and Resources

Includes hugo for static site generation

Includes lftp and openssh client for uploading, e.g. via sftp with a private key

# Example `gitlab-ci.yml`

```
image: registry.gitlab.com/kissaki/docker-hugo:latest

stages:
  - test
  - build
  - deploy

test:
  stage: test
  script:
  - hugo
  except:
  - master

build:
  stage: build
  script:
  - hugo
  artifacts:
    paths:
    - public
    expire_in: 5 days
  only:
  - master

deploy_staging:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script:
  - which ssh-agent
  - which lftp
  - eval $(ssh-agent -s)
  - echo "$SFTP_PRIVKEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo "$SFTP_KNOWNHOSTS" > ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  script:
    - lftp -c "open -u $SFTP_USERNAME,$SFTP_PASSWORD -p $SFTP_PORT sftp://$SFTP_HOST ; mirror --reverse public "
  environment:
    name: staging
    url: https://test.example.org
  only:
  - master

deploy_prod:
  stage: deploy
  script:
    - lftp -e "mirror -R public public" -u $FTP_USERNAME,$FTP_PASSWORD $FTP_HOST
  environment:
    name: production
    url: https://example.org
  when: manual
  only:
  - master
```
