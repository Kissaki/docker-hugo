FROM alpine:latest

ENV HUGO_VERSION 0.54.0
# For checksum of file hugo_<version>_Linux-64bit.tar.gz see hugo_<version>_checksums.txt
ENV HUGO_SHA 76f90287c12a682c9137b85146c406be410b2b30b0df7367f02ee7c4142bb416

# Install HUGO
RUN set -eux && \
    apk add --update --no-cache \
      ca-certificates \
      openssl \
      git \
      openssh-client \
      lftp \
      p7zip \
      && \
  wget -O ${HUGO_VERSION}.tar.gz https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz && \
  echo "${HUGO_SHA}  ${HUGO_VERSION}.tar.gz" | sha256sum -c && \
  tar xf ${HUGO_VERSION}.tar.gz && mv hugo* /usr/bin/hugo && \
  rm -r ${HUGO_VERSION}.tar.gz && \
  rm -f /etc/apk/cache/* && \
  hugo version

EXPOSE 1313

CMD ["/usr/local/bin/hugo"]
